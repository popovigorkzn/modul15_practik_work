﻿#include <iostream>
using namespace std;

void findOddNumbers(int N, int OddOrNot)
{
	for (int i = OddOrNot; i <= N; i +=2)
	{
		cout << i << "\n";
	}
}

int main()
{
	int N;
	cout << "Enter your number : ";
	cin >> N;

	cout << "Even numbers are : \n";
	findOddNumbers(N, 2);

	cout << "Even numbers are : \n";
	findOddNumbers(N, 1);
}